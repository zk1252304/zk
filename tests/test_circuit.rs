use std::time::Instant;
use std::thread;
use std::sync::Arc;
use std::fs::OpenOptions;
use std::io::Write;

use rand::Rng;

use rustzk::app::aes::*;
use rustzk::app::aes::*;
use rustzk::field::*;
use rustzk::channel::*;
use rustzk::circuit::*;
use rustzk::statistic::*;
use rustzk::zkp::*;
use rustzk::tape::*;
use rustzk::util::*;

const N_PARTY: usize = 16;
const VOLE_N: usize = 64;
const LOG_RHO: usize = 4;
const LOG_LAYER_RATIO: usize = 1;
// 0 for plain_VDP, 1 for mpc_VDP, 2 for Virgo's VPD
const VPD_MODE: usize = 0;
const N_MPC_ITER: usize = 1;

fn circuit<T: Field>() {
    let log_len = 16;
    let log_l = 16 + LOG_RHO;

    stats_reset_all();

    let n_ldt_iter = get_n_ldt_iter(
        log_len, T::SIZE * 8,
        (1 << LOG_RHO) / (if VPD_MODE == 2 { 2 } else { 1 })
        - if VPD_MODE == 1 { 0 } else { 1 });
    println!("LDT num of queries: {}", n_ldt_iter);

    let tape_len: usize = 32;

    let mseed = Seed::random();

    let mut lc = LocalVithChannel::<T>::new(&mseed, tape_len, VOLE_N, N_PARTY);
    let mut lc0 = PLocalVithChannel::<T>::new(&mut lc);
    let mut lc1 = PLocalVithChannel::<T>::new(&mut lc);

    let mut c = Circuit::<T>::random(3, 17, 17, 16);
    let mut inputs: Vec<T> = vec![];
    for _ in 0..(1 << c.layers[0].bit_len) {
        inputs.push(T::random());
    }
    c.eval(&inputs);

    let mut c_v = c.clone();
    let outputs = c.layers.last().unwrap().values.clone();

    let mut coset = Coset::<T>::init(log_l, LOG_RHO, LOG_LAYER_RATIO);
    coset.precompute(n_ldt_iter, VPD_MODE != 2);

    let coset = Arc::new(coset);

    let coset_p = coset.clone();
    let p_thread = thread::spawn(move || {
        let lc = &mut *lc0;
        let mut prover = Prover::<T, LocalVithChannel<T>>::new(
            16, &mut c, &inputs, lc);
        if VPD_MODE == 0 {
            prover.prove_plain(n_ldt_iter, &coset_p);
        } else if VPD_MODE == 1 {
            prover.prove_mpcvpd(&coset_p, n_ldt_iter, N_PARTY, N_MPC_ITER);
        } else {
            prover.prove(n_ldt_iter, &coset_p);
        }
    });

    let coset_v = coset.clone();
    let v_thread = thread::spawn(move || {
        let lc = &mut *lc1;
        let mut verifier = Verifier::<T, LocalVithChannel<T>>::new(
            16, &mut c_v, lc);
        let res =
            if VPD_MODE == 0 {
                verifier.verify_plain(&outputs, N_PARTY, tape_len, n_ldt_iter, &coset_v)
            } else if VPD_MODE == 1 {
                verifier.verify_mpcvpd(&outputs, &coset_v, N_PARTY, tape_len, n_ldt_iter, N_MPC_ITER)
            } else {
                verifier.verify(&outputs, N_PARTY, tape_len, n_ldt_iter, &coset_v)
            };
        assert!(res);
    });

    v_thread.join().unwrap();
    p_thread.join().unwrap();
    lc.close();

    let mut file = OpenOptions::new()
        .write(true)
        .create(true)
        .append(true)
        .open(std::format!("log/random_{}.log", VPD_MODE))
        .unwrap();
    statistic(&mut file, 16, n_ldt_iter, N_MPC_ITER);
}

#[test]
fn test_circuit() {
    circuit::<GF2p192>();
}
