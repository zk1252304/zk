use rand::Rng;
use std::time::Instant;
use sha3::{Digest, Sha3_256};


fn mt(log_size: usize) {

    let mut input = vec![0u8; (512 / 8) << log_size];
    let start = Instant::now();
    for k in 0..log_size {
        for i in 0..(1 << (log_size - k)) {
            let mut hasher = Sha3_256::new();
            hasher.update(&input[(512 / 8 * i)..(512 / 8 * (i + 1))]);
            hasher.finalize_into((&mut input[(256 / 8 * i)..(256 / 8 * (i + 1))]).into());
        }
    }
    let elapsed = start.elapsed();
    let in_ms = elapsed.as_micros();
    println!("mt {} time: {}", log_size, in_ms as f64 / 1000.0);
}


#[test]
fn benchmark() {
    println!("");
    for i in 10..31 {
        mt(i);
    }
}
