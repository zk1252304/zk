use std::sync::Arc;

use crate::field::*;
use crate::circuit::*;
use crate::gkr::*;
use crate::util::*;


pub fn build_circuit<'a, T: Field>(log_member_size: usize) -> Circuit<'a, T> {
    let mut depth = 0;
    let mut c = Circuit::<T>::new(depth);

    {
        c.layers.push(Layer::<T>::default());
        let layer = &mut c.layers[depth];
        let log_size = log_member_size + 1;
        layer.bit_len = log_size;
        // TODO: XXX useless gates at input layer
        // for i in 0..((1 << log_member_size) + 1) {
        for i in 0..(1 << log_size) {
            layer.gates.push(Gate::default());
            layer.gates[i].gtype = GateType::IN;
            layer.gates[i].inputs.push(i as u32);
            layer.values.push(T::from(0));
        }
        depth += 1;
    }

    {
        c.layers.push(Layer::<T>::default());
        let layer = &mut c.layers[depth];
        let log_size = log_member_size;
        layer.bit_len = log_size;
        // TODO: XXX useless gates at input layer
        for i in 0..(1 << log_size) {
            layer.gates.push(Gate::default());
            layer.gates[i].gtype = GateType::SUB;
            layer.gates[i].inputs.push(i as u32);
            layer.gates[i].inputs.push((1 << log_size) as u32);
        }
        depth += 1;
    }

    for j in 0..log_member_size {
        c.layers.push(Layer::<T>::default());
        let layer = &mut c.layers[depth];
        let log_size = log_member_size - j - 1;
        layer.bit_len = log_size;
        // TODO: XXX useless gates at input layer
        for i in 0..(1 << log_size) {
            layer.gates.push(Gate::default());
            layer.gates[i].gtype = GateType::MUL;
            layer.gates[i].inputs.push((2 * i) as u32);
            layer.gates[i].inputs.push((2 * i + 1) as u32);
        }
        depth += 1;
    }

    c.depth = depth;
    c
}

pub fn gen_member_witness<T: Field>(log_member_size: usize) -> Vec<T> {
    let mut res = vec![];
    for i in 0..(1 << log_member_size) {
        res.push(T::random());
    }
    let mid = res[1];
    res.push(mid);
    for i in 1..(1 << log_member_size) {
        res.push(T::random());
    }
    res
}

/* pub fn gen_sha3mt_witness<T: Field>(inputs: &Vec<[u64; 8]>, log_leaf_size: usize) -> Vec<T> {
    let mut witness = Vec::with_capacity(512 << (log_leaf_size + 1));
    for i in 0..inputs.len() {
        for w in 0..8 {
            let mut inp = inputs[i][w] as usize;
            for _ in 0..64 {
                witness.push(T::from(inp & 1));
                inp >>= 1;
            }
        }
    }
    let mut inputs = inputs.clone();

    for d in 0..=log_leaf_size {
        let width = 1 << (log_leaf_size - d);
        let mut state = vec![[0u64; 25]; width];
        for i in 0..width {
            for j in 0..8 {
                state[i][j] = inputs[i][j];
            }

            for r in 0..24 {
                // θ step
                let mut c = [0u64; 5];
                for x in 0..=4 {
                    c[x] =
                        state[i][x * 5 + 0] ^
                        state[i][x * 5 + 1] ^
                        state[i][x * 5 + 2] ^
                        state[i][x * 5 + 3] ^
                        state[i][x * 5 + 4];
                }
                let mut a = [0u64; 25];
                for x in 0..=4 {
                    for y in 0..=4 {
                        a[x * 5 + y] = state[i][x * 5 + y] ^ c[(x + 4) % 5] ^ rot(c[(x + 1) % 5], 1);
                    }
                }
                // if d == 0 && r == 0 {
                //     for i in 0..25 {
                //         println!("a {:02}: {:016x}", i, a[i]);
                //     }
                // }

                // ρ and π steps
                let mut b = [0u64; 25];
                for x in 0..=4 {
                    for y in 0..=4 {
                        b[y * 5 + (2 * x + 3 * y) % 5] = rot(a[x * 5 + y], ROTATE[x][y]);
                    }
                }
                // if d == 0 && r == 0 {
                //     for i in 0..25 {
                //         println!("b {:02}: {:016x}", i, b[i]);
                //     }
                // }

                // χ step
                for x in 0..=4 {
                    for y in 0..=4 {
                        state[i][x * 5 + y] =
                            b[x * 5 + y] ^
                            ((!b[((x + 1) % 5) * 5 + y]) & b[((x + 2) % 5) * 5 + y]);
                    }
                }

                // ι step
                state[i][0] ^= RC[r] as u64;

                // if d == 0 && r == 22 {
                //     for j in 0..25 {
                //         println!("b {:02}: {:016x}", j, state[i][j]);
                //     }
                // }
            }
            inputs[i / 2][(i % 2) * 4 + 0] = state[i][0];
            inputs[i / 2][(i % 2) * 4 + 1] = state[i][1];
            inputs[i / 2][(i % 2) * 4 + 2] = state[i][2];
            inputs[i / 2][(i % 2) * 4 + 3] = state[i][3];
            for k in 0..4 {
                let mut inp = state[i][k] as usize;
                for _ in 0..64 {
                    witness.push(T::from(inp & 1));
                    inp >>= 1;
                }
            }
        }
    }
    for _ in 0..256 {
        witness.push(T::from(0));
    }

    witness
}
*/
